package osdaemon

import (
	"fmt"
	"os/exec"
)

// Bash run bash comand
func Bash(comand string, folder ...string) (string, error) {
	cmd := exec.Command("bash", "-c", comand)
	if len(folder) > 0 {
		cmd.Dir = folder[0]
	}
	salid, err := cmd.Output()
	salida := string(salid)
	return salida, err
}

// BashLog run bash comand and add log
func BashLog(comand string, SaveLog func(string, string, string)) (string, error) {
	salida, err := Bash(comand)
	if err != nil {
		SaveLog("ComandError", comand, err.Error())
	}
	SaveLog("ComandOutput", comand, salida)

	return salida, err
}

// BashFolderLog run bash comand in folder and add log
func BashFolderLog(folder string, comand string, SaveLog func(string, string, string)) (string, error) {
	return BashLog(fmt.Sprintf("cd %s;%s", folder, comand), SaveLog)
}
