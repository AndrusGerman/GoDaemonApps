package databasedaemon

import (
	"fmt"
	"os"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

var db *gorm.DB
var dblog *gorm.DB

// GetDB is get connection db
func GetDB() *gorm.DB {
	var err error
	if db != nil {
		return db
	}
	db, err = gorm.Open("sqlite3", "databasedaemon/daemon.db")
	if err != nil {
		fmt.Println("Error DB", err)
		os.Exit(1)
	}
	return db
}

// GetDBLog is get connection db
func GetDBLog() *gorm.DB {
	var err error
	if dblog != nil {
		return db
	}
	dblog, err = gorm.Open("sqlite3", "databasedaemon/daemonLog.db")
	if err != nil {
		fmt.Println("Error DB", err)
		os.Exit(1)
	}
	return dblog
}

// ResetDBConnection reset db
func ResetDBConnection(call func()) {
	db.Close()
	call()
	GetDB()
}
