package enginedaemon

import (
	"fmt"

	"gitlab.com/AndrusGerman/GoDaemonApps/checkdaemon"
	"gitlab.com/AndrusGerman/GoDaemonApps/modelsdaemon"
	"gitlab.com/AndrusGerman/GoDaemonApps/osdaemon"
)

// UpdateApps daemon in golang
func UpdateApps(cl func(string, string)) {
	var forBuildApps = checkdaemon.GetNeedBuild(cl)
	cl("NeedBuild", fmt.Sprintf("for build '%d' apps", len(forBuildApps)))
	for _, appUpdate := range forBuildApps {
		var app, _ = modelsdaemon.GetAppByID(appUpdate.AppID)
		app.StopApp()
		cl("UpdateApps: ", fmt.Sprintf("Stop '%s'", app.AppName))
		osdaemon.Bash(appUpdate.PreBuildComand, appUpdate.AppFolder)
		osdaemon.Bash(appUpdate.BuildComand, appUpdate.AppFolder)
		cl("UpdateApps: ", fmt.Sprintf("Run '%s'", app.AppName))
		app.RunApp()
	}
	cl("UpdateApps", "Complete")
}
