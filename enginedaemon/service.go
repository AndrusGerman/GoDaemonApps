package enginedaemon

import (
	"gitlab.com/AndrusGerman/GoDaemonApps/checkdaemon"
	"gitlab.com/AndrusGerman/GoDaemonApps/modelsdaemon"
)

// GetServiceStatus in database
func GetServiceStatus() []modelsdaemon.SystemctlService {
	var apps = new(modelsdaemon.SystemctlService).GetAll()
	apps = checkdaemon.RunningService(apps)
	return apps
}
