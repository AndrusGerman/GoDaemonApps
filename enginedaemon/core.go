package enginedaemon

import "time"

// CoreEngine run
func CoreEngine() {
	go func() {
		for {
			time.Sleep(time.Second)
			var apps = GetAppsStatus()
			for _, val := range apps {
				if !CheckAppsRunning && val.NotReanudate == false {
					go val.RunApp()
				}
			}
		}
	}()
}
