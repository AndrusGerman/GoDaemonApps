package enginedaemon

import (
	"gitlab.com/AndrusGerman/GoDaemonApps/checkdaemon"
	"gitlab.com/AndrusGerman/GoDaemonApps/modelsdaemon"
)

// GetAppsStatus in database
func GetAppsStatus() []modelsdaemon.App {
	var apps = new(modelsdaemon.App).GetAll()
	apps = checkdaemon.RunningApps(apps)
	return apps
}
