package serverdaemon

import (
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/AndrusGerman/GoDaemonApps/modelsdaemon"
)

func routeReanudateApp(ctx *gin.Context) {
	id := ctx.Param("id")
	val, err := modelsdaemon.GetAppByID(id)
	if err != nil {
		ctx.JSON(400, err.Error())
		return
	}
	val.NotReanudate = false
	val.Update(val.ID)
}

func routeStopApp(ctx *gin.Context) {
	id := ctx.Param("id")
	val, err := modelsdaemon.GetAppByID(id)
	if err != nil {
		ctx.JSON(400, err.Error())
		return
	}
	val.NotReanudate = true
	val.Update(val.ID)
	time.Sleep(time.Second)
	val.StopApp()
}
