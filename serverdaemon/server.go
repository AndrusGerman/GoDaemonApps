package serverdaemon

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/AndrusGerman/GoDaemonApps/modelsdaemon"
	"gitlab.com/AndrusGerman/GoDaemonApps/serverdaemon/controllerdaemon"
)

var rootPassword string

// RunServer start server
func RunServer(p string) {
	rootPassword = p
	gin.SetMode(gin.ReleaseMode)
	server := gin.New()
	server.LoadHTMLGlob("serverdaemon/views/*")
	// Routes
	server.GET("/", routeMain)
	server.GET("/logs", routeGetLogs)
	server.GET("/update_all_apps", routeUpdateAllApps)
	server.GET("/stop_all_apps", routeStopAllApps)
	server.GET("/reanudate_all_apps", routeReanudateAllApps)
	api := server.Group("/api")
	// Routes apps
	api.GET("/apps", controllerdaemon.RouteGetAllApps)
	api.POST("/apps", controllerdaemon.RouteCreateApps)
	api.DELETE("/apps/:id", controllerdaemon.RouteDeleteApps)
	api.PUT("/apps/:id", controllerdaemon.RouteUpdateApps)
	// Routes appsUpdate
	api.GET("/apps_update", controllerdaemon.RouteGetAllAppUpdate)
	api.POST("/apps_update", controllerdaemon.RouteCreateAppUpdate)
	api.DELETE("/apps_update/:id", controllerdaemon.RouteDeleteAppUpdate)
	api.PUT("/apps_update/:id", controllerdaemon.RouteUpdateAppUpdate)
	// Routes service
	api.GET("/service", controllerdaemon.RouteGetAllSystemctlService)
	api.POST("/service", controllerdaemon.RouteCreateSystemctlService)
	api.DELETE("/service/:id", controllerdaemon.RouteDeleteSystemctlService)
	api.PUT("/service/:id", controllerdaemon.RouteUpdateSystemctlService)
	// Routes service
	api.GET("/app_service", controllerdaemon.RouteGetAllAppService)
	api.POST("/app_service", controllerdaemon.RouteCreateAppService)
	api.DELETE("/app_service/:id", controllerdaemon.RouteDeleteAppService)
	api.PUT("/app_service/:id", controllerdaemon.RouteUpdateAppService)
	// Run Server
	err := server.Run(":8097")
	if err != nil {
		modelsdaemon.SaveLog("ServerStop:", "Server", err.Error())
	} else {
		modelsdaemon.SaveLog("ServerStop:", "Server", "Stop Server")
	}
}
