package serverdaemon

import (
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/AndrusGerman/GoDaemonApps/enginedaemon"
	"gitlab.com/AndrusGerman/GoDaemonApps/modelsdaemon"
)

func routeMain(ctx *gin.Context) {
	var df = enginedaemon.CheckAppsRunning
	enginedaemon.CheckAppsRunning = false
	time.Sleep(time.Second)
	//Init scan
	apps := enginedaemon.GetAppsStatus()
	services := enginedaemon.GetServiceStatus()
	ctx.JSON(200, gin.H{
		"CheckAppsRunning": df,
		"Apps":             apps,
		"Service":          services,
		"Routes": gin.H{
			"update_all_apps":    "For update all apps",
			"stop_all_apps":      "For stop all apps",
			"reanudate_all_apps": "For reanudate all apps",
			"app_stop/:id":       "For stop app",
			"app_reanudate/:id":  "For stop app",
			"logs":               "Get log daemon",
		},
	})
	//On close
	enginedaemon.CheckAppsRunning = df
}

func routeGetLogs(ctx *gin.Context) {
	ctx.JSON(200, new(modelsdaemon.Log).GetAll())
}
