package controllerdaemon

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/AndrusGerman/GoDaemonApps/modelsdaemon"
)

// RouteGetAllSystemctlService route
func RouteGetAllSystemctlService(ctx *gin.Context) {
	var value = new(modelsdaemon.SystemctlService).GetAll()
	ctx.JSON(200, value)
}

// RouteCreateSystemctlService route
func RouteCreateSystemctlService(ctx *gin.Context) {
	var value = new(modelsdaemon.SystemctlService)
	if err := ctx.BindJSON(value); err != nil {
		ctx.JSON(400, err)
		return
	}
	if err := value.Create(); err != nil {
		ctx.JSON(400, err)
		return
	}
	ctx.JSON(200, value)
}

// RouteDeleteSystemctlService route
func RouteDeleteSystemctlService(ctx *gin.Context) {
	var value = new(modelsdaemon.SystemctlService)
	id := ctx.Param("id")
	if err := value.Delete(id); err != nil {
		ctx.JSON(400, err)
		return
	}
	ctx.JSON(200, value)
}

// RouteUpdateSystemctlService route
func RouteUpdateSystemctlService(ctx *gin.Context) {
	var value = new(modelsdaemon.SystemctlService)
	id := ctx.Param("id")
	if err := ctx.BindJSON(value); err != nil {
		ctx.JSON(400, err)
		return
	}
	if err := value.Update(id); err != nil {
		ctx.JSON(400, err)
		return
	}
	ctx.JSON(200, value)
}
