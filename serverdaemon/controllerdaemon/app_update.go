package controllerdaemon

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/AndrusGerman/GoDaemonApps/modelsdaemon"
)

// RouteGetAllAppUpdate route
func RouteGetAllAppUpdate(ctx *gin.Context) {
	var value = new(modelsdaemon.AppUpdate).GetAll()
	ctx.JSON(200, value)
}

// RouteCreateAppUpdate route
func RouteCreateAppUpdate(ctx *gin.Context) {
	var value = new(modelsdaemon.AppUpdate)
	if err := ctx.BindJSON(value); err != nil {
		ctx.JSON(400, err)
		return
	}
	if err := value.Create(); err != nil {
		ctx.JSON(400, err)
		return
	}
	ctx.JSON(200, value)
}

// RouteDeleteAppUpdate route
func RouteDeleteAppUpdate(ctx *gin.Context) {
	var value = new(modelsdaemon.AppUpdate)
	id := ctx.Param("id")
	if err := value.Delete(id); err != nil {
		ctx.JSON(400, err)
		return
	}
	ctx.JSON(200, value)
}

// RouteUpdateAppUpdate route
func RouteUpdateAppUpdate(ctx *gin.Context) {
	var value = new(modelsdaemon.AppUpdate)
	id := ctx.Param("id")
	if err := ctx.BindJSON(value); err != nil {
		ctx.JSON(400, err)
		return
	}
	if err := value.Update(id); err != nil {
		ctx.JSON(400, err)
		return
	}
	ctx.JSON(200, value)
}
