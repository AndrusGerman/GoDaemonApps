package controllerdaemon

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/AndrusGerman/GoDaemonApps/modelsdaemon"
)

// RouteGetAllAppService route
func RouteGetAllAppService(ctx *gin.Context) {
	var value = new(modelsdaemon.AppService).GetAll()
	ctx.JSON(200, value)
}

// RouteCreateAppService route
func RouteCreateAppService(ctx *gin.Context) {
	var value = new(modelsdaemon.AppService)
	if err := ctx.BindJSON(value); err != nil {
		ctx.JSON(400, err)
		return
	}
	if err := value.Create(); err != nil {
		ctx.JSON(400, err)
		return
	}
	ctx.JSON(200, value)
}

// RouteDeleteAppService route
func RouteDeleteAppService(ctx *gin.Context) {
	var value = new(modelsdaemon.AppService)
	id := ctx.Param("id")
	if err := value.Delete(id); err != nil {
		ctx.JSON(400, err)
		return
	}
	ctx.JSON(200, value)
}

// RouteUpdateAppService route
func RouteUpdateAppService(ctx *gin.Context) {
	var value = new(modelsdaemon.AppService)
	id := ctx.Param("id")
	if err := ctx.BindJSON(value); err != nil {
		ctx.JSON(400, err)
		return
	}
	if err := value.Update(id); err != nil {
		ctx.JSON(400, err)
		return
	}
	ctx.JSON(200, value)
}
