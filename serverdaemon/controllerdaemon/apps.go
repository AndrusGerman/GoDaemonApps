package controllerdaemon

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/AndrusGerman/GoDaemonApps/modelsdaemon"
)

// RouteGetAllApps route
func RouteGetAllApps(ctx *gin.Context) {
	var value = new(modelsdaemon.App).GetAll()
	ctx.JSON(200, value)
}

// RouteCreateApps route
func RouteCreateApps(ctx *gin.Context) {
	var value = new(modelsdaemon.App)
	if err := ctx.BindJSON(value); err != nil {
		ctx.JSON(400, err)
		return
	}
	if err := value.Create(); err != nil {
		ctx.JSON(400, err)
		return
	}
	ctx.JSON(200, value)
}

// RouteDeleteApps route
func RouteDeleteApps(ctx *gin.Context) {
	var value = new(modelsdaemon.App)
	id := ctx.Param("id")
	if err := value.Delete(id); err != nil {
		ctx.JSON(400, err)
		return
	}
	ctx.JSON(200, value)
}

// RouteUpdateApps route
func RouteUpdateApps(ctx *gin.Context) {
	var value = new(modelsdaemon.App)
	id := ctx.Param("id")
	if err := ctx.BindJSON(value); err != nil {
		ctx.JSON(400, err)
		return
	}
	if err := value.Update(id); err != nil {
		ctx.JSON(400, err)
		return
	}
	ctx.JSON(200, value)
}
