package serverdaemon

import (
	"encoding/json"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/AndrusGerman/GoDaemonApps/enginedaemon"
)

func routeReanudateAllApps(ctx *gin.Context) {
	enginedaemon.CheckAppsRunning = true
	ctx.JSON(200, "Complete")
}

func routeStopAllApps(ctx *gin.Context) {
	enginedaemon.CheckAppsRunning = false
	time.Sleep(time.Second)
	apps := enginedaemon.GetAppsStatus()
	for _, val := range apps {
		val.StopApp()
	}
	ctx.JSON(200, "Complete")
}

func routeUpdateAllApps(ctx *gin.Context) {
	ctx.Writer.Header().Set("content-type", "application/json; charset=utf-8")
	ctx.Writer.WriteHeader(http.StatusOK)
	enginedaemon.UpdateApps(func(found string, log string) {
		dt, _ := json.Marshal(gin.H{
			"log":   log,
			"found": found,
		})
		ctx.Writer.Write(dt)
		ctx.Writer.Flush()
	})
}
