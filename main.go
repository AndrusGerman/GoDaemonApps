package main

import (
	"gitlab.com/AndrusGerman/GoDaemonApps/enginedaemon"
	"gitlab.com/AndrusGerman/GoDaemonApps/modelsdaemon"
	"gitlab.com/AndrusGerman/GoDaemonApps/serverdaemon"
)

func main() {
	modelsdaemon.Init()
	modelsdaemon.SaveLog("DaemonStart:", "Daemon", "Start Daemon 8097")
	enginedaemon.CoreEngine()
	serverdaemon.RunServer(passwordRegister)
}
