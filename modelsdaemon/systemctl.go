package modelsdaemon

import "github.com/jinzhu/gorm"

// SystemctlService service models
type SystemctlService struct {
	gorm.Model
	Name         string
	SytemctlName string
	IsRunning    bool `gorm:"-"`
}

// Create by model
func (ctx *SystemctlService) Create() error {
	return db.Create(ctx).Error
}

// Update by model
func (ctx *SystemctlService) Update(id interface{}) error {
	return db.Update(ctx, id).Error
}

// Delete by model
func (ctx *SystemctlService) Delete(id interface{}) error {
	return db.Delete(ctx, id).Error
}

// GetAll by model
func (SystemctlService) GetAll() []SystemctlService {
	var value []SystemctlService
	db.Find(&value)
	return value
}
