package modelsdaemon

import (
	"fmt"

	"github.com/jinzhu/gorm"
	"gitlab.com/AndrusGerman/GoDaemonApps/osdaemon"
)

// App struct for apps
type App struct {
	gorm.Model
	AppName       string
	ProcessName   string
	ProcessFolder string
	RunningComand string
	NotReanudate  bool
	IsRunning     bool `gorm:"-"`
}

// StopApp by model
func (ctx *App) StopApp() {
	osdaemon.Bash(fmt.Sprintf("killall -15 %s", ctx.ProcessName))
}

// Create by model
func (ctx *App) Create() error {
	return db.Create(ctx).Error
}

// Update by model
func (ctx *App) Update(id interface{}) error {
	return db.Update(ctx, id).Error
}

// Delete by model
func (ctx *App) Delete(id interface{}) error {
	return db.Delete(ctx, id).Error
}

// GetAll by model
func (App) GetAll() []App {
	var value []App
	db.Find(&value)
	return value
}

// RunApp by model
func (ctx *App) RunApp() {
	ctx.IsRunning = true
	osdaemon.BashFolderLog(ctx.ProcessFolder, ctx.RunningComand, SaveLog)
	ctx.IsRunning = false
}

// GetAppByID by id
func GetAppByID(id interface{}) (*App, error) {
	var val = new(App)
	err := db.Find(val, id).Error
	return val, err
}
