package modelsdaemon

import (
	"gitlab.com/AndrusGerman/GoDaemonApps/databasedaemon"
)

var db = databasedaemon.GetDB()
var dblog = databasedaemon.GetDBLog()

// Init migrate all data
func Init() {
	LogAutoMigrate()
	db.AutoMigrate(&App{})
	db.AutoMigrate(&AppService{})
	db.AutoMigrate(&AppUpdate{})
	db.AutoMigrate(&SystemctlService{})
	db.AutoMigrate(&User{})
}
