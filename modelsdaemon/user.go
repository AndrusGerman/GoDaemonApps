package modelsdaemon

import (
	"github.com/jinzhu/gorm"
)

// User model
type User struct {
	gorm.Model
	Nick     string
	Password string
}

// Create by model
func (ctx *User) Create() error {
	return db.Create(ctx).Error
}

// Update by model
func (ctx *User) Update(id interface{}) error {
	return db.Update(ctx, id).Error
}

// Delete by model
func (ctx *User) Delete(id interface{}) error {
	return db.Delete(ctx, id).Error
}

// GetAll by model
func (User) GetAll() []User {
	var value []User
	db.Find(&value)
	return value
}
