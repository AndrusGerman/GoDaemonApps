package modelsdaemon

import "github.com/jinzhu/gorm"

// AppService is relation dependenci f
type AppService struct {
	gorm.Model
	SystemctlServiceID uint
	AppID              uint
}

// Create by model
func (ctx *AppService) Create() error {
	return db.Create(ctx).Error
}

// Update by model
func (ctx *AppService) Update(id interface{}) error {
	return db.Update(ctx, id).Error
}

// Delete by model
func (ctx *AppService) Delete(id interface{}) error {
	return db.Delete(ctx, id).Error
}

// GetAll by model
func (AppService) GetAll() []App {
	var value []App
	db.Find(&value)
	return value
}
