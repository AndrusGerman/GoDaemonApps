package modelsdaemon

import (
	"fmt"

	"github.com/jinzhu/gorm"
)

// Log content
type Log struct {
	gorm.Model
	LogType     string
	LogProccess string
	LogContent  string
}

// LogAutoMigrate migrate
func LogAutoMigrate() {
	dblog.AutoMigrate(&Log{})
}

// Create log
func (ctx *Log) Create() error {
	return dblog.Create(ctx).Error
}

// SaveLog save log
func SaveLog(typelg string, proccess string, contentlg string) {
	d := (&Log{
		LogContent:  contentlg,
		LogType:     typelg,
		LogProccess: proccess,
	})
	fmt.Printf("LogType='%v' LogProccess='%v' LogContent='%v'\n", d.LogType, d.LogProccess, d.LogContent)
	d.Create()
}

// Delete by model
func (ctx *Log) Delete(id interface{}) error {
	return dblog.Delete(ctx, id).Error
}

// GetAll by model
func (Log) GetAll() []Log {
	var value []Log
	dblog.Find(&value)
	return value
}
