package modelsdaemon

import "github.com/jinzhu/gorm"

// AppUpdate models
type AppUpdate struct {
	gorm.Model
	AppID           uint
	GitHTTPS        string
	AppFolder       string
	PreUpdateComand string
	PreBuildComand  string
	BuildComand     string
}

// Create by model
func (ctx *AppUpdate) Create() error {
	return db.Create(ctx).Error
}

// Update by model
func (ctx *AppUpdate) Update(id interface{}) error {
	return db.Update(ctx, id).Error
}

// Delete by model
func (ctx *AppUpdate) Delete(id interface{}) error {
	return db.Delete(ctx, id).Error
}

// GetAll by model
func (AppUpdate) GetAll() []AppUpdate {
	var value []AppUpdate
	db.Find(&value)
	return value
}
