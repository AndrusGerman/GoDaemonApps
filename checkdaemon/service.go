package checkdaemon

import (
	"strings"

	"gitlab.com/AndrusGerman/GoDaemonApps/modelsdaemon"
	"gitlab.com/AndrusGerman/GoDaemonApps/osdaemon"
)

// RunningService check
func RunningService(apps []modelsdaemon.SystemctlService) []modelsdaemon.SystemctlService {
	for ind := range apps {
		apps[ind].IsRunning = isRunningService(apps[ind].SytemctlName)
	}
	return apps
}

func isRunningService(nameSystemctl string) bool {
	commando := "systemctl is-enabled " + nameSystemctl
	salida, _ := osdaemon.Bash(
		commando,
	)
	if strings.Contains(salida, "disabled") || strings.Contains(salida, "Failed") {
		return false
	}
	if strings.Contains(salida, "enabled") || strings.Contains(salida, "static") {
		return true
	}

	return false
}
