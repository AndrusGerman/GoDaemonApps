package checkdaemon

import (
	"fmt"
	"os"
	"strings"

	"gitlab.com/AndrusGerman/GoDaemonApps/modelsdaemon"
	"gitlab.com/AndrusGerman/GoDaemonApps/osdaemon"
)

// GetNeedBuild get all for update
func GetNeedBuild(cl func(string, string)) []modelsdaemon.AppUpdate {
	cl("NeedBuild", "Loading")
	var forBuild []modelsdaemon.AppUpdate
	allApps := new(modelsdaemon.AppUpdate).GetAll()
	cl("NeedBuild", fmt.Sprintf("Get '%d' apps", len(allApps)))
	for ind := range allApps {
		if _, err := os.Stat(allApps[ind].AppFolder); os.IsNotExist(err) {
			cl("UpdateError:", "App Not found folder '"+allApps[ind].AppFolder+"'")
			if allApps[ind].GitHTTPS != "" {
				// path/to/whatever does not exist
				os.MkdirAll(allApps[ind].AppFolder, 0777)
				clone := "cd ..;git clone " + allApps[ind].GitHTTPS
				s, _ := osdaemon.Bash(
					clone,
					allApps[ind].AppFolder,
				)
				cl("UpdateClone", clone+" = '"+s+"'")
			}
		}
		// Run preUpdate Comand
		osdaemon.Bash(
			allApps[ind].PreUpdateComand,
			allApps[ind].AppFolder,
		)
		// Add for build
		if ok, sl := isEnableBuild(allApps[ind].AppFolder); ok {
			cl("NeedBuild", fmt.Sprintf("'%d:%s' Add to compile", allApps[ind].AppID, sl))
			forBuild = append(forBuild, allApps[ind])
		} else {
			cl("NeedBuild", fmt.Sprintf("'%d:%s' Not Add to compile", allApps[ind].AppID, sl))
		}
	}
	return allApps
}

func isEnableBuild(folder string) (bool, string) {
	output, _ := osdaemon.BashFolderLog(folder, "git pull", modelsdaemon.SaveLog)
	return !strings.Contains(output, "Already up-to-date."), output
}
