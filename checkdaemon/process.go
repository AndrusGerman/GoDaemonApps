package checkdaemon

import (
	"gitlab.com/AndrusGerman/GoDaemonApps/modelsdaemon"
	"gitlab.com/AndrusGerman/GoDaemonApps/osdaemon"
)

// RunningApps check
func RunningApps(apps []modelsdaemon.App) []modelsdaemon.App {
	for ind := range apps {
		apps[ind].IsRunning = isRunningApp(apps[ind].ProcessName)
	}
	return apps
}

func isRunningApp(comand string) bool {
	commando := "ps -ef | awk '$8==\"" + comand + "\" {print $2}'"
	salida, _ := osdaemon.Bash(
		commando,
	)
	if salida == "" {
		return false
	}
	return true
}
